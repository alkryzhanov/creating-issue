FROM node:14.17-alpine

WORKDIR /creating-issue

COPY package.json .
RUN npm install

COPY createIssue.js .
COPY $INTRODUCTION_FILE .

ENTRYPOINT ["node", "/creating-issue/createIssue.js"]
