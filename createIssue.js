const fs = require("fs");
const axios = require("axios");

const gitlabToken = process.env.GITLAB_TOKEN;
const projectId = process.env.PROJECT_ID;
const gitlabApiUrl = process.env.GITLAB_API_URL;
const fileName = "./1-introduction.md";

const gitlab = axios.create({
  baseURL: gitlabApiUrl,
  headers: { "PRIVATE-TOKEN": gitlabToken },
});

async function createOrUpdateIssue() {
  const lastDashIndex = fileName.lastIndexOf("-");
  const issueId = parseInt(
    fileName.slice(lastDashIndex - 1, lastDashIndex + 1)
  );
  const title = fileName
    .substring(lastDashIndex + 1, fileName.lastIndexOf(".md"))
    .replace(/^\w/, (c) => c.toUpperCase());

  let templateFile;
  try {
    templateFile = fs.readFileSync(fileName, "utf8");
  } catch (error) {
    console.info(`Failed to read file "${fileName}"`, error);
    return;
  }

  try {
    const response = await gitlab.get(
      `/projects/${projectId}/issues/${issueId}`
    );
    const issue = response.data;
    console.log(`Updating issue ${issueId}`);
    await gitlab.put(`/projects/${projectId}/issues/${issue.iid}`, {
      title,
      description: templateFile,
    });
  } catch (error) {
    if (error.response && error.response.status === 404) {
      console.log(`Creating new issue`);
      const response = await gitlab.post(`/projects/${projectId}/issues`, {
        title,
        description: templateFile,
      });
      const issue = response.data;
      console.log(`Created new issue with ID ${issue.iid}`);
    } else {
      console.info(
        `Failed to update/create issue for file "${fileName}"`,
        error
      );
    }
  }
}

createOrUpdateIssue().catch((err) => console.info(err));
